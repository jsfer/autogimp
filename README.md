# AutoGimp
A simple desktop GUI written in C++ with Gtkmm. Allows for easy batch processing by linking to GIMP libraries.
*Note:* currently only supported on Linux systems

# Compilation
Ensure the following system dependencies are installed:
- PkgConfig
- cmake
- a c++ compiler
- gtkmm-3.0
- gimp-2.0

Setup the project and run the following commands:
```bash
git clone https://gitlab.com/jsfer/autogimp  &&  cd autogimp
mkdir build  &&  cd build
cmake ..
make
```

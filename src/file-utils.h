#pragma once

#include <filesystem>

namespace AG {
namespace Util {

// return absolute path of installed program
std::filesystem::path get_program_name();

// return the absolute path of installed data directory
std::filesystem::path get_data_dir();

}
}

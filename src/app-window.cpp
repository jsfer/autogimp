#include "app-window.h"

namespace AG {

AppWindow::AppWindow()
{

    set_title("autogimp");
    set_default_size(1280, 720);
    set_resizable(false);
}

AppWindow::~AppWindow()
{

}

}

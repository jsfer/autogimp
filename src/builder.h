#pragma once

#include <gtkmm-3.0/gtkmm.h>
#include <glibmm/refptr.h>
#include <string>

namespace AG {
namespace Util {

Glib::RefPtr<Gtk::Builder> create_builder(std::string const path);
bool check_glade_file(std::string const path);


}
}

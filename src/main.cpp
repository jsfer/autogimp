#include "app-window.h"

int main(int argc, char **argv) {
    auto app = Gtk::Application::create(argc, argv);
    auto appwin = AG::AppWindow();
    return app->run(appwin);
}

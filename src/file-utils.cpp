#include "file-utils.h"

#include <iostream>

namespace AG {
namespace Util {

std::filesystem::path get_program_name()
{
    std::string exe_path = std::getenv("AUTOGIMP_INSTALLED_DIR");
    if (exe_path.empty()) {
        std::cerr << "AUTOGIMP_INSTALLED_DIR not set\n";
    }

    return exe_path;
}

std::filesystem::path get_data_dir()
{
    std::string data_dir = std::getenv("AUTOGIMP_DATA_DIR");
    if (data_dir.empty()) {
        std::cerr << "AUTOGIMP_DATA_DIR not set\n";
    }

    return data_dir;
}

}
}

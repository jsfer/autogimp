#pragma once

#include <gtkmm-3.0/gtkmm.h>
#include <glibmm/refptr.h>

namespace AG {

class AppWindow : public Gtk::ApplicationWindow {
public:
    AppWindow();
    ~AppWindow();

private:
};

}

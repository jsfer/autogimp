#include "builder.h"

#include <filesystem>

namespace AG {
namespace Util {

Glib::RefPtr<Gtk::Builder> create_builder(std::string const path)
{
    if (!check_glade_file(path)) {
        g_error("Could not load Glade file: %s", path.c_str());
        throw;
    }

    return Gtk::Builder::create_from_file(path);
}

bool check_glade_file(std::string const path)
{
    auto glade_files = std::filesystem::path(); // get the res/glade path
    for (auto const &d : std::filesystem::directory_iterator{glade_files}) {
        if (d.path().filename() == path + ".glade") {
            return true;
        }
    }
    return false;
}

}
}
